﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Client
    {
        private string clientFName;
        private string clientLName;
        private string clientMail;
        private string clientPNum;

        public Client()
        {

        }

        public string ClientFName
        {
            get { return clientFName; }
            set { clientFName = value; }
        }

        public string ClientLName
        {
            get { return clientLName; }
            set { clientLName = value; }
        }

        public string ClientMail
        {
            get { return clientMail; }
            set { clientMail = value; }
        }

        public string ClientPhone
        {
            get { return clientPNum; }
            set { clientPNum = value; }
        }
    }
}