﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class TshirtOrderForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void addLocationValidator(object source, ServerValidateEventArgs addDL)
        {
            // validation code for the validator will be here
            //I based on some of your code(adding an item to list)->pizzatoppings.Add(topping.Text);
            //loop was based here: https://docs.microsoft.com/en-us/dotnet/api/system.web.ui.webcontrols.checkboxlist?redirectedfrom=MSDN&view=netframework-4.7.2 
            //referece for string to char conversion: http://www.java2s.com/Code/CSharp/Data-Types/Convertstringtochar.htm

            for (int ctr = 0; ctr < addDesignLoc.Items.Count; ctr++)
            {
                if (addDesignLoc.Items[ctr].Selected)
                {
                    // add current value to a temp variable to covert string to char
                    string charTemp = addDesignLoc.Items[ctr].Value;
                    //get the value of the mainDESIGN 
                    string mainTempLoc = mainDesign.SelectedItem.Value.ToString();
                    //convert string to char
                    Char mainDesignLocVal = System.Convert.ToChar(mainTempLoc[0]);
                    //convert string to char
                    Char addDesignLocTemp = System.Convert.ToChar(charTemp[0]);
                    //check if user selected the same location as same design, if true, trigger the validator                  
                    if (addDesignLocTemp.Equals(mainDesignLocVal) == false)
                    {
                        addDL.IsValid = true;
                    }
                    else
                    {
                        addDL.IsValid = false;
                        return;//exit loop f it returns false
                    }
                }
            }
        }

        protected void shippingMethodValidator(object source, ServerValidateEventArgs pickup)
        {
            //check if pickup is valid on your province
            // String compare .Equals based here: https://docs.microsoft.com/en-us/dotnet/csharp/how-to/compare-strings
            // I based the custom validator on your custom validator code

            string method = clientProvince.SelectedValue.ToString();

            if (shipMethod.SelectedValue == "PICKUP")
            {
                if (method.Equals("ON") == true)
                {
                    pickup.IsValid = true;
                }
                else
                {
                    pickup.IsValid = false;
                }
            }
        }

        protected void subOrder(object sender, EventArgs e)
        {
            orderSummary.InnerHtml = ""; //clear innerHtml everytime submit button is clicked

            // I copied this from your code for server side validation to make sure everything else is valid
            if (!Page.IsValid)
            {
                orderSummary.InnerHtml = ""; //clear innerHtml if page is not valid
                return;
            }

            //Declare Variables

            //Client Details
            string firstName = clientFirstName.Text.ToString();
            string lastName = clientLastName.Text.ToString();
            string emailAddress = clientEmail.Text.ToString();
            string clientPhone = clientPhoneNumber.Text.ToString();

            //Shirt Details
            string shirtTypes = shirtType.SelectedItem.Value.ToString();
            string shirtColors = shirtColor.SelectedItem.Value.ToString();
            string shirtSizes = shirtSize.SelectedItem.Value.ToString();
            string mainDesignText = mainDesign.SelectedItem.Text.ToString();
            string tempLoc = mainDesign.SelectedItem.Value.ToString();
            Char mainDesignLoc = System.Convert.ToChar(tempLoc[0]); //referece: http://www.java2s.com/Code/CSharp/Data-Types/Convertstringtochar.htm
            Char addLocTemp;
            List<Char> addLoc = new List<Char> { };
            List<string> addLocString = new List<string> { };
            List<string> addDesignFile = new List<string> { };
            string addInstructions = designInstructions.Text.ToString();
            int orderQuantity = int.Parse(shirtQuantity.Text);
            

            //Payment Details
            string clientShipAddress = clientDeliveryAddress.Text.ToString();
            string clientShipCity = clientCity.Text.ToString();
            string clientShipProvince = clientProvince.SelectedItem.Value.ToString();
            string clientZip = clientZipCode.Text.ToString();
            string paymentMethods = paymentMethod.SelectedItem.Value.ToString();
            string shipMethods = shipMethod.SelectedItem.Value.ToString();

            /*
             * shorthand if else for c#:
                https://stackoverflow.com/questions/6073563/shorthand-if-statements-c-sharp
             */
            Boolean trackEmail = subscriptionMailChk.Checked == true ? true : false;
            Boolean trackText = subscriptionTxtChk.Checked == true ? true : false;


            // retrieve checked checkboxes
            /* sources: 
                I based on some of your code (adding an item to list) -> pizzatoppings.Add(topping.Text);
                loop was based here: https://docs.microsoft.com/en-us/dotnet/api/system.web.ui.webcontrols.checkboxlist?redirectedfrom=MSDN&view=netframework-4.7.2 
                referece for string to char conversion: http://www.java2s.com/Code/CSharp/Data-Types/Convertstringtochar.htm
            */
            for (int ctr = 0; ctr < addDesignLoc.Items.Count; ctr++)
            {
                if (addDesignLoc.Items[ctr].Selected)
                {
                    // add current value to a temp variable to covert string to char
                    string charTemp = addDesignLoc.Items[ctr].Value;
                    //get the Text of the checkbox for receipt purposes
                    string addLocTempText = addDesignLoc.Items[ctr].Text;
                    //convert string to char
                    addLocTemp = System.Convert.ToChar(charTemp[0]);
                                    
                    addLoc.Add(addLocTemp); //add to char array
                    addLocString.Add(addLocTempText); //add to string array

                }
            }


            // check the uploaded files. Thisdoes not upload or save the image, I skipped the file upload function because I only need to take the file name
            // resource : https://forums.asp.net/t/2101927.aspx?Validation+for+Multiple+File+upload+using+asp+net+C+web+forms
            if (clientFiles.HasFiles)
            {
                foreach (HttpPostedFile postedFile in clientFiles.PostedFiles)
                {
                    //each valid file will be added in the List
                    addDesignFile.Add(postedFile.FileName);
                }
            }

            // start instantiating objects

            //Client
            Client newClient = new Client();
            newClient.ClientFName = firstName;
            newClient.ClientLName = lastName;
            newClient.ClientMail = emailAddress;
            newClient.ClientPhone = clientPhone;

            //Shirt
            Shirt newShirt = new Shirt();
            newShirt.ShirtType = shirtTypes;
            newShirt.ShirtColor = shirtColors;
            newShirt.ShirtSize = shirtSizes;
            newShirt.MainDesignLoc = mainDesignLoc;
            newShirt.MainDesignLocText = mainDesignText;
            newShirt.ShirtOtherLoc = addLoc;
            newShirt.ShirtOtherLocText = addLocString;
            newShirt.AddInstructions = addInstructions;
            newShirt.ShirtFiles = addDesignFile;
            newShirt.ShirtQuantity = orderQuantity;

            //Logistics
            Logistics newLogistic = new Logistics();
            newLogistic.ClientAddress = clientShipAddress;
            newLogistic.ClientCity = clientShipCity;
            newLogistic.ClientProvince = clientShipProvince;
            newLogistic.ClientZipCode = clientZip;
            newLogistic.PaymentMethod = paymentMethods;
            newLogistic.DeliveryMethod = shipMethods;
            newLogistic.TextTracking = trackText;
            newLogistic.EmailTracking = trackEmail;

            //Order
            OrderShirt orderShirt = new OrderShirt(newClient, newShirt, newLogistic);

            //Output Receipt:
            orderSummary.InnerHtml = orderShirt.PrintSummary();
        }
    }
}