﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Logistics
    {
        private string clientDelAddress;
        private string clientDelCity;
        private string clientDelProvince;
        private string clientDelZip;
        private string payMethod;
        private string delMethod;
        private Boolean txtTrack;
        private Boolean emailTrack;

        public Logistics()
        {
        
        }

        public string ClientAddress
        {
            get { return clientDelAddress; }
            set { clientDelAddress = value; }
        }

        public string ClientCity
        {
            get { return clientDelCity; }
            set { clientDelCity = value; }
        }

        public string ClientProvince
        {
            get { return clientDelProvince; }
            set { clientDelProvince = value; }
        }

        public string ClientZipCode
        {
            get { return clientDelZip; }
            set { clientDelZip = value; }
        }

        public string PaymentMethod
        {
            get { return payMethod; }
            set { payMethod = value; }
        }

        public string DeliveryMethod
        {
            get { return delMethod; }
            set { delMethod = value; }
        }

        public Boolean TextTracking
        {
            get { return txtTrack; }
            set { txtTrack = value; }
        }

        public Boolean EmailTracking
        {
            get { return emailTrack; }
            set { emailTrack = value; }
        }
    }
}